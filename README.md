Yii2 Glide
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-glide --prefer-dist
```
or add
```
"code2magic/yii2-glide": "*"
```

to the `require` section of your `composer.json` file.

## Configuration

This extension is supposed to be used with [composer-config-plugin].

Else look files for cofiguration example:

* [src/config/glide.php]
* [src/config/params.php]
* [src/config/storage.php]

[composer-config-plugin]:   https://github.com/hiqdev/composer-config-plugin
[src/config/glide.php]:     src/config/glide.php
[src/config/params.php]:    src/config/params.php
[src/config/storage.php]:   src/config/storage.php

## Usage
