<?php

namespace code2magic\glide\components;


use yii\base\InvalidConfigException;

/**
 * Interface IGlide
 * @package code2magic\glide\components
 *
 * @param $source        \League\Flysystem\FilesystemInterface
 * @param $cache         \League\Flysystem\FilesystemInterface
 * @param $server        \League\Glide\Server
 * @param $httpSignature \League\Glide\Signatures\Signature
 * @param $urlBuilder    \League\Glide\Urls\UrlBuilderFactory
 */
interface IGlide
{
    /**
     * @param $image
     * @return mixed
     */
    public function getImageSourceUrl($image);

    /**
     * @param array $params
     * @param bool $scheme
     *
     * @return bool|string
     * @throws InvalidConfigException
     */
    public function createSignedUrl(array $params, $scheme = false);

    public function getGlideThumbnail($image, $preset = null, array $params = []);

    /**
     * Get configured server.
     *
     * @return \League\Glide\Server
     */
    public function getServer();

    /**
     * Get noimage placeholder
     *
     * @return string
     */
    public function getNoImage();
}