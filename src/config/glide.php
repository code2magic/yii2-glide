<?php
return [
    'container' => [
        'singletons' => [
            \code2magic\glide\components\IGlide::class => [
                'class' => \code2magic\glide\components\Glide::class,
                'sourcePath' => '@storage/web/source',
                'cachePath' => '@storage/web/cache',
                'urlManager' => 'urlManagerStorage',
                'maxImageSize' => $params['glide.maxImageSize'],
                'signKey' => $params['glide.signKey'],
                'no_image' => $params['glide.no_image'],
            ],
        ],
    ],
    'components' => [
        'glide' => \code2magic\glide\components\IGlide::class,
    ]
];
