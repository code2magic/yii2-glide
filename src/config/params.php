<?php
return [
    'glide.maxImageSize' => null,
    'glide.signKey' => null,
    'glide.no_image' => null,
];
