<?php
return [
    'controllerMap' => [
        'glide' => \code2magic\glide\controllers\GlideController::class
    ],
    'components' => [
        'urlManager' => [
            'class' => yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'glide' => ['pattern' => 'cache/<path:(.*)>', 'route' => 'glide/index', 'encodeParams' => false],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'glide/error'
        ],
    ],
];
